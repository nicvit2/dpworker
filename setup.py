# add ./support dir to python path when venv is activated
import os

cwd = os.getcwd()
targetpath = os.path.split(cwd)[0]
targetpath = os.path.join(targetpath, 'lib/python3.6/site-packages/this_env.pth')

try:
    with open(targetpath, 'w+') as f:
        f.write("../../../src/support")
except:
    print("Something went wrong modify this script to match your system specs.")
