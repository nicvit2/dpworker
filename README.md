#### Instruction to reproduce python3 environment

create a python3 venv
create a src folder within the venv and clone this repo to src

- __requirements.txt__: python standard pip requirements format
- __dpipeline dirs__: contains etl groups to be run according to design doc
- __setup.py__: to be run only once (after virtualenv creation)
