# iterate fasta files in ./fasta_docs folder
# parse the content and insert into mongo

import os
import re
from pymongo import MongoClient

uri_a = "mongodb://useradmin:ro5_p4assw0rd@172.31.35.7:27017/admin"
client_a = MongoClient(uri_a)
db_a = client_a['pubchem_eng']
collection_a = db_a['IC50_targets']



base_dir = '/home/ubuntu/py/insert_gi2acesssions/src/fasta_docs'
list_all = os.listdir(base_dir)



def headerparse(header_str):
    header_dict = {}
    tab_split_list = header_str.split()

    accession_and_tag = tab_split_list[0]
    accession = re.search("(?<=>sp\|)(.*?)(?=\|)", accession_and_tag)
    try:
        accession = accession.group(0)
    except:
        print("\n\n")
        print("ACCESSION regex exception:\n")
        print(header_str)
        print("\n\n")
    tag = re.search("\w+$", accession_and_tag)
    try:
        tag = tag.group(0)
    except:
        print("\n\n")
        print("TAG regex exception:\n")
        print(header_str)
        print("\n\n")

    header_str_rejoined = ' '.join(tab_split_list[1:])
    protein_name = re.search("(?<=RecName: Full=)(.*?)(?=;|$)", header_str_rejoined)
    try:
        protein_name = protein_name.group(0)
    except:
        protein_name = header_str_rejoined
    #################################################
    ###   PARSE WITH REGEXES ABOVE HERE   ###########
    #################################################
    header_dict['accession'] = accession
    header_dict['tag'] = tag
    header_dict['recognised_name'] = protein_name
    return header_dict




for fastas_batch in list_all:
    fastas_batch_fullpath = os.path.join(base_dir, fastas_batch)

    with open(fastas_batch_fullpath, 'r') as infile:
        batch_str = repr(infile.read())
        single_fastas_list = batch_str.split('\\n\\n')
        single_fastas_list =single_fastas_list[:-1]

        for fasta_string in single_fastas_list:
            lines = fasta_string.split('\\n')

            header = lines[0]
            fasta_str = ''.join(lines[1:])

            out_doc = headerparse(header)
            out_doc['fasta_str'] = fasta_str
            out_doc['fasta_len'] = len(fasta_str)

            collection_a.insert_one(out_doc)

client_a.close()
