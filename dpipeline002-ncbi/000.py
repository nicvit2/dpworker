from pymongo import MongoClient

uri_a = "mongodb://useradmin:ro5_p4assw0rd@172.31.35.7:27017/admin"
client_a = MongoClient(uri_a)
db_a = client_a['pubchem_eng']
collection_a1 = db_a['IC50_assays']

cur_a = collection_a1.distinct("PC_AssaySubmit.assay.descr.target.mol_id")

cnt = 1

for i in cur_a:

    gi = int(i)

    with open('protein_gis.txt', 'a+') as outfile:
        outfile.write(repr(gi) + "\n")

    print(f"Processed count: {cnt} of 4681")
    cnt += 1

client_a.close()
