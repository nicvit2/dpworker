from pymongo import MongoClient

uri = "mongodb://useradmin:ro5_p4assw0rd@172.31.35.7:27017/admin"
client = MongoClient(uri)
db = client['pubchem_eng']
collection = db['ncbi_accessions']

with open('gi2acc.txt') as infile:
    line = infile.readline()
    while line:

        no_endline = line.strip()
        list = no_endline.split()

        if len(list) != 0:
            outdoc = {'gi': int(list[0]), 'ncbi_accession': str(list[1])}

            collection.insert_one(outdoc)

            print(outdoc)

            line = infile.readline()

client.close()