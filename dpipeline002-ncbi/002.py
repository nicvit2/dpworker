# LIMIT: 3 requests per second
# but can put comma separated list of protein accession ids (do batches of 100)
# https://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db=protein&id=P33261.3&rettype=fasta

from pymongo import MongoClient
import requests
import time

uri_a = "mongodb://useradmin:ro5_p4assw0rd@172.31.35.7:27017/admin"
client_a = MongoClient(uri_a)
db_a = client_a['pubchem_eng']
collection_a = db_a['ncbi_accessions']

cur_a = collection_a.find().sort('gi', 1)

batch_n = 1
batch_list = []

for acc_doc in cur_a:

    gi_id = acc_doc['gi']
    accession_id = acc_doc['ncbi_accession']
    batch_list.append(accession_id)

    if len(batch_list) == 100:
        batch_str = ','.join(str(i) for i in batch_list)
        request_url = f'https://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db=protein&id={batch_str}&rettype=fasta'
        response_file = requests.get(request_url)
        outpath = f'/home/ubuntu/py/insert_gi2acesssions/src/fasta_docs/batch{batch_n}.fasta'
        open(outpath, 'wb').write(response_file.content)

        print(f"Batch #:   {batch_n}   of 46\nLast GI of batch:   {gi_id}")

        if batch_n % 3 == 0:
            time.sleep(1)
            print("\n\n")

        batch_list = []
        batch_n += 1

    # last batch
    if batch_n == 47:

        for acc_doc in cur_a:
            gi_id = acc_doc['gi']
            accession_id = acc_doc['ncbi_accession']
            batch_list.append(accession_id)

        batch_str = ','.join(str(i) for i in batch_list)
        request_url = f'https://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db=protein&id={batch_str}&rettype=fasta'
        response_file = requests.get(request_url)
        outpath = f'/home/ubuntu/py/insert_gi2acesssions/src/fasta_docs/batch{batch_n}.fasta'
        open(outpath, 'wb').write(response_file.content)
        print(f"\n\nLAST BATCH inserted!!!\nLast GI of batch: {gi_id}")
        cur_a.close()
        break

client_a.close()