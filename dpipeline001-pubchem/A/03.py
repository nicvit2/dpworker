import os
import pickle
from zipfile import ZipFile


with open('list_all_ftp.pkl', 'rb') as f:
    list_all_A = pickle.load(f)


for input_from_A in list_all_A:

    with ZipFile('/home/ubuntu/lake/pubchem/A/{}'.format(input_from_A), 'r') as zipObj:
        zipObj.extractall('/home/ubuntu/lake/pubchem/B/')

        print("\n   *** Unzipped: {}\n".format(input_from_A))
