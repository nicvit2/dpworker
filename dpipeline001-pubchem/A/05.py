import os
from pymongo import MongoClient
import codecs
import json

base_dir = '/home/ubuntu/lake/pubchem/C'
list_all_C = os.listdir(base_dir)

print("\n*** Loading files names (assays ids aid.json) and their sizes.\n")

pairs = []
for json_assay_document in list_all_C:
    location = os.path.join(base_dir, json_assay_document)
    size = os.path.getsize(location)
    pairs.append((size, location))

print("\n*** Sorting for insertion order ... ...\n")

pairs.sort(key=lambda s: s[0])

print("\n*** Sorted files for insertion order.\n")

print("\n*** Connecting to MongoDB ... ...\n")

client = MongoClient(
		'172.31.35.7',
		username='useradmin',
		password='ro5_p4assw0rd')
db = client['pubchem']
collection = db['assays']

for size_path in pairs:
    path = size_path[1]

    print("\n***  Mongo inserting assay of size: {} ... ...\n".format(size_path[0]))

    with open(path) as json_data:
        data_dict = json.load(json_data)


        # move json subsetting logic here

        collection.insert_one(data_dict)
