import pickle
from ftplib import FTP
from zipfile import ZipFile

list_already_extracted = []
with open('list_all_ftp.pkl', 'rb') as f:
    list_tobe_extracted = pickle.load(f)

for input_from_ftp in list_tobe_extracted:
    ftp = FTP('ftp.ncbi.nlm.nih.gov')
    ftp.login()
    ftp.cwd('/pubchem/Bioassay/JSON/')

    with open('/home/ubuntu/lake/pubchem/A/{}'.format(input_from_ftp), 'wb') as f:
        ftp.retrbinary('RETR {}'.format('/pubchem/Bioassay/JSON/' + input_from_ftp), f.write)

    ftp.quit() # Do I need this ???

    print("\n   *** Transfered: {}\n".format(input_from_ftp))

    # Save the state of the extraction process
    list_already_extracted.append(input_from_ftp)
    list_tobe_extracted = list(set(list_tobe_extracted) - set(list_already_extracted))
    with open('list_tobe_extracted.pkl', 'wb') as f:
        pickle.dump(list_tobe_extracted, f)
    ############################################################################
