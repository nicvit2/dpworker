import pickle
from ftplib import FTP

ftp = FTP('ftp.ncbi.nlm.nih.gov')
ftp.login()
ftp.cwd('/pubchem/Bioassay/JSON/')

# pickle list of all files
list_all_ftp = ftp.nlst()
list_all_ftp.sort()

with open('list_all_ftp.pkl', 'wb') as f:
    pickle.dump(list_all_ftp, f)
