from pymongo import MongoClient
import psycopg2
import numpy as np
import pandas as pd
import time
import pprint
pp = pprint.PrettyPrinter(indent = 1)

# ro5_doc_store01
uri01 = "mongodb://useradmin:ro5_p4assw0rd@172.31.35.7:27017/admin"
client01 = MongoClient(uri01)
db01 = client01['pubchem_eng']
collection01 = db01['IC50_assays']

# ro5_doc_store02
uri02 = "mongodb://admin:ro5_p4assw0rd@172.31.43.207:27017/admin"
client02 = MongoClient(uri02)
db02 = client02['pubchem_eng']
collection02 = db02['assays_table']

MEASURES_SUBSET = ["PubChem Standard Value", "Standard Type", "Standard Relation", "Standard Value", "Standard Units", "Activity Comment", "Data Validity Comment"]
measures_lookup = pd.DataFrame(MEASURES_SUBSET, columns = ['measures'])

for x in collection01.find():

    these_measures = []
    for r in x['PC_AssaySubmit']['assay']['descr']['results']:
        these_measures.append([r['name'], r['tid']])
    tmp_df = pd.DataFrame(these_measures, columns = ['measures', 'tid'])
    df_theseMeasures = pd.merge(measures_lookup, tmp_df, on = 'measures', how = 'left')

    SUBSTANCES_PART_LIST = []
    for d in x['PC_AssaySubmit']['data']:
        SUBSTANCES_PART = {}
        SUBSTANCES_PART['sid'] = d['sid']
        SUBSTANCES_PART['outcome'] = d.get('outcome', "NULL")
        these_datadata = []
        for dd in d['data']:
            tmp_tidValue = []
            tmp_tidValue.append(dd['tid'])
            for key, value in dd['value'].items():
                tmp_tidValue.append(value)
            these_datadata.append(tmp_tidValue)
        df_theseDataData = pd.DataFrame(these_datadata, columns = ['tid', 'value'])
        df_theseDataData = pd.merge(df_theseMeasures, df_theseDataData, on = 'tid', how = 'left')
        df_theseDataData = df_theseDataData.replace(np.nan, "NULL", regex = True)
        SUBSTANCES_PART['pubc_std_value'] = df_theseDataData.loc[df_theseDataData['measures'] == "PubChem Standard Value", 'value'].item()
        SUBSTANCES_PART['std_type'] = df_theseDataData.loc[df_theseDataData['measures'] == "Standard Type", 'value'].item()
        SUBSTANCES_PART['std_relation'] = df_theseDataData.loc[df_theseDataData['measures'] == "Standard Relation", 'value'].item()
        SUBSTANCES_PART['std_value'] = df_theseDataData.loc[df_theseDataData['measures'] == "Standard Value", 'value'].item()
        SUBSTANCES_PART['std_units'] = df_theseDataData.loc[df_theseDataData['measures'] == "Standard Units", 'value'].item()
        SUBSTANCES_PART['activity_comment'] = df_theseDataData.loc[df_theseDataData['measures'] == "Activity Comment", 'value'].item()
        SUBSTANCES_PART['data_validity_comment'] = df_theseDataData.loc[df_theseDataData['measures'] == "Data Validity Comment", 'value'].item()
        date = d.get('date', False)
        if date == False:
            insert_date = "NULL"
        else:
            date = date.get('std', False)
            if date == False:
                insert_date = "NULL"
            else:
                year = date.get('year', "2000")
                month = str(date.get('month', "01"))
                if len(month) == 1:
                    month = str(0) + month
                day = str(date.get('day', "01"))
                if len(day) == 1:
                    day = str(0) + day
                insert_date = "-".join([str(year), str(month), str(day)])
        SUBSTANCES_PART['date'] = insert_date
        SUBSTANCES_PART['assay_id'] = x['PC_AssaySubmit']['assay']['descr']['aid']['id']
        SUBSTANCES_PART['assay_name'] = x['PC_AssaySubmit']['assay']['descr'].get('name', "NULL")
        SUBSTANCES_PART['activity_outcome_method'] = x['PC_AssaySubmit']['assay']['descr'].get('activity_outcome_method', "NULL")
        SUBSTANCES_PART['project_category'] = x['PC_AssaySubmit']['assay']['descr'].get('project_category', "NULL")
        SUBSTANCES_PART_LIST.append(SUBSTANCES_PART)

    for s in SUBSTANCES_PART_LIST:
        for t in x['PC_AssaySubmit']['assay']['descr']['target']:
            s['ncbi_gi'] = t.get('mol_id', "NULL")

    collection02.insert_many(SUBSTANCES_PART_LIST)

client01.close()
client02.close()
