from pymongo import MongoClient

# ro5_doc_store02
uri02 = "mongodb://admin:ro5_p4assw0rd@172.31.43.207:27017/admin"
client02 = MongoClient(uri02)
db02 = client02['pubchem_eng']
collection01 = db02['assays_table']
collection02 = db02['long_substances']
collection03 = db02['assays_table_final']

for a in collection01.find().sort('_id', 1):
    assay_id = a['assay_id']
    sid = a['sid']
    # print({'aid': assay_id, 'sid': sid})
    matched_substance = collection02.find_one({'aid': str(assay_id), 'sid': str(sid)})
    new_doc = a
    try:
        new_doc['cid'] = matched_substance['cid']
        collection03.insert_one(new_doc)
    except:
        print(matched_substance)
    # try:
    #     new_doc['cid'] = matched_substance['cid']
    # except:
    #     print("matched_substance error")
    #     print(matched_substance)
    # try:
    #     collection03.insert_one(new_doc)
    # except:
    #     print("insert error")
    #     print(a)
