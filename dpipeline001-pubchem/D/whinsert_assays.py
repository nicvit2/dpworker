from pymongo import MongoClient
import psycopg2

# ro5_doc_store02
uri02 = "mongodb://admin:ro5_p4assw0rd@172.31.43.207:27017/admin"
client02 = MongoClient(uri02)
db02 = client02['pubchem_eng']
collection = db02['assays_table_final_really']


con = psycopg2.connect(dbname = "bioactivity",
                        user="postgres",
                        password="ro5p455word",
                        host = "172.31.36.87")


for a in collection.find():

    aid = int(a.get('assay_id'))
    assay_name = a.get('assay_name')
    outcome_method = str(a.get('activity_outcome_method'))
    project_category = str(a.get('project_category'))

    cid = int(a.get('cid'))
    sid = int(a.get('sid'))
    substance_outcome = a.get('outcome')
    pubc_std_value = a.get('pubc_std_value')
    if pubc_std_value == "NULL":
        pubc_std_value = None
    std_type = a.get('std_type')
    std_relation = a.get('std_relation')
    std_value = a.get('std_value')
    if std_value == "NULL":
        std_value = None
    std_units = a.get('std_units')
    activity_comment = a.get('activity_comment')
    data_validity_comment = a.get('data_validity_comment')
    date = a.get('date')

    ncbi_gi = a.get('ncbi_gi')
    ncbi_accession = a.get('ncbi_accession')

    cur = con.cursor()
    cur.execute("INSERT INTO warehouse.assays (aid, assay_name, outcome_method, project_category, cid, sid, substance_outcome,\
                    pubch_std_value, std_type, std_relation, std_value, std_units, activity_comment,\
                    data_validity_comment, date, ncbi_gi, ncbi_accession)\
                    VALUES(%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)",
                    (aid, assay_name, outcome_method, project_category, cid, sid, substance_outcome,
                    pubc_std_value, std_type, std_relation, std_value, std_units, activity_comment,
                    data_validity_comment, date, ncbi_gi, ncbi_accession))
    con.commit()
    cur.close()

con.close()
