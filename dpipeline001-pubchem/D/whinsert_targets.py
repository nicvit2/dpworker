from pymongo import MongoClient
import psycopg2


def cast_int(hashed_int):
    if hashed_int is not None:
        hashed_int = int(hashed_int)
    return hashed_int

def cast_float(hashed_float):
    if hashed_float is not None:
        hashed_float = float(hashed_float)
    return hashed_float

def cast_str(hashed_str):
    if hashed_str is not None:
        hashed_str = str(hashed_str)
    return hashed_str


# ro5_doc_store01
uri01 = "mongodb://useradmin:ro5_p4assw0rd@172.31.35.7:27017/admin"
client01 = MongoClient(uri01)
db01 = client01['pubchem_eng']
collection = db01['IC50_targets']

# warehouse
con = psycopg2.connect(dbname = "bioactivity",
                        user="postgres",
                        password="ro5p455word",
                        host = "172.31.36.87")


for a in collection.find():

    fasta_len = cast_int(a.get('fasta_len'))

    ncbi_accession = cast_str(a.get('accession', None))
    ncbi_tag = cast_str(a.get('tag', None))
    recognised_name = cast_str(a.get('recognised_name', None))
    fasta_str = cast_str(a.get('fasta_str', None))

    sql_statement = """
        INSERT INTO warehouse.targets
        (ncbi_accession, ncbi_tag, recognised_name, fasta_str, fasta_len)
        VALUES
        (%s, %s, %s, %s, %s)
        """

    cur = con.cursor()
    cur.execute(sql_statement, (ncbi_accession, ncbi_tag, recognised_name, fasta_str, fasta_len))
    con.commit()
    cur.close()

con.close()
