from pymongo import MongoClient

# ro5_doc_store01
uri01 = "mongodb://useradmin:ro5_p4assw0rd@172.31.35.7:27017/admin"
client01 = MongoClient(uri01)
db01 = client01['pubchem_eng']
collection01 = db01['IC50_substances']

# ro5_doc_store02
uri02 = "mongodb://admin:ro5_p4assw0rd@172.31.43.207:27017/admin"
client02 = MongoClient(uri02)
db02 = client02['pubchem_eng']
collection02 = db02['long_substances']

for s in collection01.find():
    new_docs_list = []
    for substance in s['substances']:
        new_doc = {'aid': s['aid'], 'sid': substance['sid'], 'cid': substance['cid']}
        new_docs_list.append(new_doc)
    try:
        collection02.insert_many(new_docs_list)
    except:
        print(new_docs_list)
