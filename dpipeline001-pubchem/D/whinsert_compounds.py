from pymongo import MongoClient
import psycopg2


def cast_int(hashed_int):
    if hashed_int is not None:
        hashed_int = int(hashed_int)
    return hashed_int

def cast_float(hashed_float):
    if hashed_float is not None:
        hashed_float = float(hashed_float)
    return hashed_float

def cast_str(hashed_str):
    if hashed_str is not None:
        hashed_str = str(hashed_str)
    return hashed_str


# ro5_doc_store01
uri01 = "mongodb://useradmin:ro5_p4assw0rd@172.31.35.7:27017/admin"
client01 = MongoClient(uri01)
db01 = client01['pubchem_eng']
collection = db01['IC50_compounds']

# warehouse
con = psycopg2.connect(dbname = "bioactivity",
                        user="postgres",
                        password="ro5p455word",
                        host = "172.31.36.87")

count = 1
for c in collection.find():

    cid = cast_int(c.get('CID'))
    complexity = cast_int(c.get('Complexity', None))
    charge = cast_int(c.get('Charge', None))
    hbond_donor_count = cast_int(c.get('HBondDonorCount', None))
    hbond_acceptor_count = cast_int(c.get('HBondAcceptorCount', None))
    rotatable_bond_count = cast_int(c.get('RotatableBondCount', None))
    heavy_atom_count = cast_int(c.get('HeavyAtomCount', None))
    isotope_atom_count = cast_int(c.get('IsotopeAtomCount', None))
    atom_stereo_count = cast_int(c.get('AtomStereoCount', None))
    def_atom_stereo_count = cast_int(c.get('DefinedAtomStereoCount', None))
    undef_atom_stereo_count = cast_int(c.get('UndefinedAtomStereoCount', None))
    bond_stereo_count = cast_int(c.get('BondStereoCount', None))
    def_bond_stereo_count = cast_int(c.get('DefinedBondStereoCount', None))
    undef_bond_stereo_count = cast_int(c.get('UndefinedBondStereoCount', None))
    covalent_unit_count = cast_int(c.get('CovalentUnitCount', None))
    feature_count_3d = cast_int(c.get('FeatureCount3D', None))
    feature_acceptor_count_3d = cast_int(c.get('FeatureAcceptorCount3D', None))
    feature_donor_count_3d = cast_int(c.get('FeatureDonorCount3D', None))
    feature_anion_count_3d = cast_int(c.get('FeatureAnionCount3D', None))
    feature_cation_count_3d = cast_int(c.get('FeatureCationCount3D', None))
    feature_ring_count_3d = cast_int(c.get('FeatureRingCount3D', None))
    feature_hydrophobe_count_3d = cast_int(c.get('FeatureHydrophobeCount3D', None))
    effective_rotor_count_3d = cast_int(c.get('EffectiveRotorCount3D', None))
    conformer_count_3d = cast_int(c.get('ConformerCount3D', None))

    molecular_weight = cast_float(c.get('MolecularWeight', None))
    x_log_p = cast_float(c.get('XLogP', None))
    exact_mass = cast_float(c.get('ExactMass', None))
    monoisotopic_mass = cast_float(c.get('MonoisotopicMass', None))
    tpsa = cast_float(c.get('TPSA', None))
    volume_3d = cast_float(c.get('Volume3D', None))
    x_steric_quadrupole_3d = cast_float(c.get('XStericQuadrupole3D', None))
    y_steric_quadrupole_3d = cast_float(c.get('YStericQuadrupole3D', None))
    z_steric_quadrupole_3d = cast_float(c.get('ZStericQuadrupole3D', None))
    conformer_model_rmsd_3d = cast_float(c.get('ConformerModelRMSD3D', None))

    molecular_formula = cast_str(c.get('MolecularFormula', None))
    canonical_smiles = cast_str(c.get('CanonicalSMILES', None))
    isometric_smiles = cast_str(c.get('IsomericSMILES', None))
    inchi = cast_str(c.get('InChI', None))
    inchi_key = cast_str(c.get('InChIKey', None))
    iupac_name = cast_str(c.get('IUPACName', None))
    fingerprint_2d = cast_str(c.get('Fingerprint2D', None))


    sql_statement = """
        INSERT INTO warehouse.compounds
        (cid, molecular_formula, molecular_weight, canonical_smiles, isometric_smiles,
        inchi, inchi_key, iupac_name, x_log_p, exact_mass, monoisotopic_mass, tpsa,
        complexity, charge, hbond_donor_count, hbond_acceptor_count, rotatable_bond_count,
        heavy_atom_count, isotope_atom_count, atom_stereo_count, def_atom_stereo_count,
        undef_atom_stereo_count, bond_stereo_count, def_bond_stereo_count, undef_bond_stereo_count,
        covalent_unit_count, volume_3d, x_steric_quadrupole_3d, y_steric_quadrupole_3d,
        z_steric_quadrupole_3d, feature_count_3d, feature_acceptor_count_3d, feature_donor_count_3d,
        feature_anion_count_3d, feature_cation_count_3d, feature_ring_count_3d, feature_hydrophobe_count_3d,
        conformer_model_rmsd_3d, effective_rotor_count_3d, conformer_count_3d, fingerprint_2d)
        VALUES
        (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s,
        %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s,
        %s, %s, %s)
        """

    cur = con.cursor()
    cur.execute(sql_statement,
        (cid, molecular_formula, molecular_weight, canonical_smiles, isometric_smiles,
        inchi, inchi_key, iupac_name, x_log_p, exact_mass, monoisotopic_mass, tpsa,
        complexity, charge, hbond_donor_count, hbond_acceptor_count, rotatable_bond_count,
        heavy_atom_count, isotope_atom_count, atom_stereo_count, def_atom_stereo_count,
        undef_atom_stereo_count, bond_stereo_count, def_bond_stereo_count, undef_bond_stereo_count,
        covalent_unit_count, volume_3d, x_steric_quadrupole_3d, y_steric_quadrupole_3d,
        z_steric_quadrupole_3d, feature_count_3d, feature_acceptor_count_3d, feature_donor_count_3d,
        feature_anion_count_3d, feature_cation_count_3d, feature_ring_count_3d, feature_hydrophobe_count_3d,
        conformer_model_rmsd_3d, effective_rotor_count_3d, conformer_count_3d, fingerprint_2d))
    con.commit()
    cur.close()
    print(count)
    count += 1

con.close()
