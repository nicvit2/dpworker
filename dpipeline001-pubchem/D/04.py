from pymongo import MongoClient

# ro5_doc_store01
uri01 = "mongodb://useradmin:ro5_p4assw0rd@172.31.35.7:27017/admin"
client01 = MongoClient(uri01)
db01 = client01['pubchem_eng']
collection01 = db01['ncbi_accessions']

# ro5_doc_store02
uri02 = "mongodb://admin:ro5_p4assw0rd@172.31.43.207:27017/admin"
client02 = MongoClient(uri02)
db02 = client02['pubchem_eng']
collection02 = db02['assays_table_final']
collection03 = db02['assays_table_final_really']

for a in collection02.find().sort('_id', 1):
    new_doc = a
    matched_target = collection01.find_one({'gi': new_doc['ncbi_gi']})
    try:
        new_doc['ncbi_accession'] = matched_target['ncbi_accession']
        collection03.insert_one(new_doc)
    except:
        print(matched_target)
