# https://pubchem.ncbi.nlm.nih.gov/rest/pug/compound/cid/1/property/MolecularFormula,MolecularWeight,CanonicalSMILES,IsomericSMILES,InChI,InChIKey,IUPACName,XLogP,ExactMass,MonoisotopicMass,TPSA,Complexity,Charge,HBondDonorCount,HBondAcceptorCount,RotatableBondCount,HeavyAtomCount,IsotopeAtomCount,AtomStereoCount,DefinedAtomStereoCount,UndefinedAtomStereoCount,BondStereoCount,DefinedBondStereoCount,UndefinedBondStereoCount,CovalentUnitCount,Volume3D,XStericQuadrupole3D,YStericQuadrupole3D,ZStericQuadrupole3D,FeatureCount3D,FeatureAcceptorCount3D,FeatureDonorCount3D,FeatureAnionCount3D,FeatureCationCount3D,FeatureRingCount3D,FeatureHydrophobeCount3D,ConformerModelRMSD3D,EffectiveRotorCount3D,ConformerCount3D,Fingerprint2D/JSON

from pymongo import MongoClient
import requests
import time

uri_a = "mongodb://useradmin:ro5_p4assw0rd@172.31.35.7:27017/admin"
client_a = MongoClient(uri_a)
db_a = client_a['pubchem_eng']
collection_a01 = db_a['unique_cids']
collection_a02 = db_a['IC50_compounds']

cur_a = collection_a01.find().sort('cid', 1)

cnt = 1
cids_batch = []

for cid_doc in cur_a:

    cid = cid_doc['cid']
    cids_batch.append(cid)

    if len(cids_batch) == 100:
        batch_str = ','.join(str(i) for i in cids_batch)

        request = f"https://pubchem.ncbi.nlm.nih.gov/rest/pug/compound/cid/{batch_str}/property/MolecularFormula,MolecularWeight,CanonicalSMILES,IsomericSMILES,InChI,InChIKey,IUPACName,XLogP,ExactMass,MonoisotopicMass,TPSA,Complexity,Charge,HBondDonorCount,HBondAcceptorCount,RotatableBondCount,HeavyAtomCount,IsotopeAtomCount,AtomStereoCount,DefinedAtomStereoCount,UndefinedAtomStereoCount,BondStereoCount,DefinedBondStereoCount,UndefinedBondStereoCount,CovalentUnitCount,Volume3D,XStericQuadrupole3D,YStericQuadrupole3D,ZStericQuadrupole3D,FeatureCount3D,FeatureAcceptorCount3D,FeatureDonorCount3D,FeatureAnionCount3D,FeatureCationCount3D,FeatureRingCount3D,FeatureHydrophobeCount3D,ConformerModelRMSD3D,EffectiveRotorCount3D,ConformerCount3D,Fingerprint2D/JSON"

        response = requests.get(request)

        response_dict = response.json()

        collection_a02.insert_many(response_dict['PropertyTable']['Properties'])


        print(f"Batch #:   {cnt}  of 4750\nLast CID of batch:   {cids_batch[-1]}\n\n")

        cids_batch = []

        if cnt%5 == 0:
            time.sleep(1)
            print("\n")
        cnt += 1


    # last batch
    if cnt == 4751:
        batch_str = ','.join(str(i) for i in cids_batch)

        request = f"https://pubchem.ncbi.nlm.nih.gov/rest/pug/compound/cid/{batch_str}/property/MolecularFormula,MolecularWeight,CanonicalSMILES,IsomericSMILES,InChI,InChIKey,IUPACName,XLogP,ExactMass,MonoisotopicMass,TPSA,Complexity,Charge,HBondDonorCount,HBondAcceptorCount,RotatableBondCount,HeavyAtomCount,IsotopeAtomCount,AtomStereoCount,DefinedAtomStereoCount,UndefinedAtomStereoCount,BondStereoCount,DefinedBondStereoCount,UndefinedBondStereoCount,CovalentUnitCount,Volume3D,XStericQuadrupole3D,YStericQuadrupole3D,ZStericQuadrupole3D,FeatureCount3D,FeatureAcceptorCount3D,FeatureDonorCount3D,FeatureAnionCount3D,FeatureCationCount3D,FeatureRingCount3D,FeatureHydrophobeCount3D,ConformerModelRMSD3D,EffectiveRotorCount3D,ConformerCount3D,Fingerprint2D/JSON"

        response = requests.get(request)

        response_dict = response.json()

        collection_a02.insert_many(response_dict['PropertyTable']['Properties'])

        print("\n\nLAST BATCH inserted!!!\n\n")

client_a.close()
