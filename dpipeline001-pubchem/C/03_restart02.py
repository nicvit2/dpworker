from pymongo import MongoClient
import requests
import time

uri_a = "mongodb://useradmin:ro5_p4assw0rd@172.31.35.7:27017/admin"
client_a = MongoClient(uri_a)
db_a = client_a['pubchem_eng']
collection_a01 = db_a['unique_cids']
collection_a02 = db_a['IC50_compounds']

# find cids that still need to be inserted:
# find where cid greater_than 126962868

# modify the script accordingly:
# number of remaining batches: (4750 - 4331) = 419
# lst batch: 420

query = { 'cid': {'$gt': 137631305 }}

cur_a = collection_a01.find(query).sort('cid', 1)

cids_batch = []
for cid_doc in cur_a:
    cid = cid_doc['cid']
    cids_batch.append(cid)

batch_str = ','.join(str(i) for i in cids_batch)

request = f"https://pubchem.ncbi.nlm.nih.gov/rest/pug/compound/cid/{batch_str}/property/MolecularFormula,MolecularWeight,CanonicalSMILES,IsomericSMILES,InChI,InChIKey,IUPACName,XLogP,ExactMass,MonoisotopicMass,TPSA,Complexity,Charge,HBondDonorCount,HBondAcceptorCount,RotatableBondCount,HeavyAtomCount,IsotopeAtomCount,AtomStereoCount,DefinedAtomStereoCount,UndefinedAtomStereoCount,BondStereoCount,DefinedBondStereoCount,UndefinedBondStereoCount,CovalentUnitCount,Volume3D,XStericQuadrupole3D,YStericQuadrupole3D,ZStericQuadrupole3D,FeatureCount3D,FeatureAcceptorCount3D,FeatureDonorCount3D,FeatureAnionCount3D,FeatureCationCount3D,FeatureRingCount3D,FeatureHydrophobeCount3D,ConformerModelRMSD3D,EffectiveRotorCount3D,ConformerCount3D,Fingerprint2D/JSON"

response = requests.get(request)

response_dict = response.json()

collection_a02.insert_many(response_dict['PropertyTable']['Properties'])

print("\n\nLAST BATCH inserted!!!\n\n")

client_a.close()
