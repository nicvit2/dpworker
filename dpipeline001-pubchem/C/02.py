# db.getCollection('substances').distinct("substances.cid").sort()
# use the above query to retrieve a sorted list of compounds to be requested
# insert the list into mongo as a new list cids_tobe

# in the following 001.py
# iterate the newly created cids_tobe, perform request and insert result into mongo directly

from pymongo import MongoClient

uri_a = "mongodb://useradmin:ro5_p4assw0rd@172.31.35.7:27017/admin"
client_a = MongoClient(uri_a)
db_a = client_a['pubchem_eng']
collection_a1 = db_a['IC50_substances']
collection_a2 = db_a['unique_cids']
# uri_b = "mongodb://admin:ro5_p4assw0rd@172.31.43.207:27017/admin"
# client_b = MongoClient(uri_b)
# db_b = client_b['pubchem']
# collection_b = db_b['cids_tobe_requested']

cur_a = collection_a1.distinct("substances.cid")

cnt = 1

for i in cur_a:
    out_doc = {'cid': int(i)}
    collection_a2.insert_one(out_doc)

    # with open('state000.log', 'w') as state:
    #     print(f"Processed count: {cnt}", file=state)

    print(f"Processed count: {cnt}")

    cnt += 1

client_a.close()
