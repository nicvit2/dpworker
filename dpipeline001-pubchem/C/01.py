# Subset assays cotaining at least one target and measuring IC50

from pymongo import MongoClient

uri_a = "mongodb://useradmin:ro5_p4assw0rd@172.31.35.7:27017/admin"
client_a = MongoClient(uri_a)
db_a1 = client_a['pubchem']
db_a2 = client_a['pubchem_eng']
collection_a1 = db_a1['assays']
collection_a2 = db_a2['IC50_assays']

query = {'$and': [
    {'PC_AssaySubmit.assay.descr.target': {'$exists': True}},
    {'PC_AssaySubmit.assay.descr.results': {'$elemMatch': {'name': "Standard Value"}}},
    {'PC_AssaySubmit.data.data': {'$elemMatch': {'value.sval': "IC50"}}}
    ]}

cur_a1 = collection_a1.find(query)

for i in cur_a1:
    print(i)
    collection_a2.insert_one(i)

client_a.close()
