import os
from zipfile import ZipFile


with open('list_all.txt', 'r') as input:

    for line in input:

        input_from_A = line.strip()

        with ZipFile('/home/ubuntu/lake/pubchem/assays_csv_raw/A/{}'.format(input_from_A), 'r') as zipObj:
            zipObj.extractall('/home/ubuntu/lake/pubchem/assays_csv_raw/B/')

            print("\n   *** Unzipped: {}\n".format(input_from_A))
