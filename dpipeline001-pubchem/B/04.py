import os
import gzip

list_all_B = os.listdir('/home/ubuntu/lake/pubchem/assays_csv_raw/B')
list_all_B.sort()

for input_from_B in list_all_B:

    list_input_from_B_children = os.listdir('/home/ubuntu/lake/pubchem/assays_csv_raw/B/{}'.format(input_from_B))
    list_input_from_B_children.sort()

    for jsongz_input_from_B_child in list_input_from_B_children:
        input = gzip.GzipFile('/home/ubuntu/lake/pubchem/assays_csv_raw/B/{}/{}'.format(input_from_B, jsongz_input_from_B_child), 'rb')
        s = input.read()
        input.close()
        output = open('/home/ubuntu/lake/pubchem/assays_csv_raw/C/{}'.format(os.path.splitext(jsongz_input_from_B_child)[0]), 'wb')
        output.write(s)
        output.close()
        print("\n   *** Gunzipped: {}\n".format(jsongz_input_from_B_child))
