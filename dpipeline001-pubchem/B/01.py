# PIPELINE INITIALIZATION LOGIC
# ftp.ncbi.nlm.nih.gov |======| ~/lake/pubchem
import pickle
from ftplib import FTP

ftp = FTP('ftp.ncbi.nlm.nih.gov')
ftp.login()
ftp.cwd('/pubchem/Bioassay/CSV/Data/')

# pickle list of all files
list_all_ftp = ftp.nlst()
list_all_ftp.sort()

with open('list_all.txt', 'a+') as outall:
    for line in list_all_ftp:
        outall.write(line + '\n')
