# Last log *** Transfered 0711001_0712000.zip
# Need to continue from here
import pickle
from ftplib import FTP
from zipfile import ZipFile

# TODO
# Connection with ftp.ncbi.nlm.nih.gov seems to work great but for robustness
# we could add logic to restart from where it left using pickled objects
with open('list_all.txt', 'r') as input:

    for line in input:

        input_from_ftp = line.strip()

        ftp = FTP('ftp.ncbi.nlm.nih.gov')
        ftp.login()
        ftp.cwd('/pubchem/Bioassay/CSV/Data/')

        with open('/home/ubuntu/lake/pubchem/assays_csv_raw/A/{}'.format(input_from_ftp), 'wb') as f:
            ftp.retrbinary('RETR {}'.format('/pubchem/Bioassay/CSV/Data/' + input_from_ftp), f.write)

        ftp.quit() # Do I need this ???

        print("\n   *** Transfered: {}\n".format(input_from_ftp))

        # Save the state of the extraction process
        with open('list_already.txt', 'a+') as state:
            state.write(input_from_ftp + '\n')
    ############################################################################
