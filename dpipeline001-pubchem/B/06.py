# read list_subset_aids and write corresponding to mongo

import pandas as pd
import time
from pymongo import MongoClient

uri = "mongodb://useradmin:ro5_p4assw0rd@172.31.35.7:27017/admin"
client = MongoClient(uri)
db = client['pubchem']
collection = db['substances']

with open('list_subset_aids.txt', 'r') as input:

    cnt = 1

    for line in input:

        input_aid = line.strip()

        open_path = f'/home/ubuntu/lake/pubchem/assays_csv_raw/C/{input_aid}.csv'

        data = pd.read_csv(open_path)

        data_subset = data.loc[data['PUBCHEM_CID'].notnull()]

        data_subset_select = data_subset[['PUBCHEM_SID', 'PUBCHEM_CID']]

        out_doc = {'aid': f'{input_aid}',
        'substances': []}

        for index, row in data_subset_select.iterrows():
            record = {'sid': repr(int(row['PUBCHEM_SID'])), 'cid': repr(int(row['PUBCHEM_CID']))}
            out_doc['substances'].append(record)

        collection.insert_one(out_doc)


        progress = cnt/223650 * 100
        print(f"Progress: {progress}%\n")
        cnt += 1


client.close()
