# Retrieve assays aids of assays in pubchem.assays_subset collection
# write aids to list file

from pymongo import MongoClient

uri = "mongodb://useradmin:ro5_p4assw0rd@172.31.35.7:27017/admin"
client = MongoClient(uri)
db = client['pubchem']
collection = db['assays_subset']
query = {}
projection = {'PC_AssaySubmit.assay.descr.aid.id': 1}

with open('list_subset_aids.txt', 'a+') as out:

    cur = collection.find(query, projection)

    for input_from_cur in cur:
        aid = input_from_cur['PC_AssaySubmit']['assay']['descr']['aid']['id']
        out.write(str(aid) + '\n')

    cur.close()

client.close()
