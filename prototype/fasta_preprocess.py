from pymongo import MongoClient

# ro5_doc_store01
uri = "mongodb://useradmin:ro5_p4assw0rd@ec2-52-14-64-216.us-east-2.compute.amazonaws.com:27017/admin"
client = MongoClient(uri)
db = client['pubchem_eng']
incollection = db['IC50_targets']
# outcollection = db['IC50_targets_preproc']
cursor = incollection.find()

for doc in cursor:
    print(doc)
    break

    # single FASTA preprocessing
    # preprocessed_doc = preprocess_fasta(doc)
    #############################################################

    # prerocessed_doc.serialise()

    # outcollection.insert_one(preprocessed_doc)

cursor.close()
client.close()
